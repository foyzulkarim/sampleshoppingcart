﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSWebApiEmpty.Models;

namespace AngularJSWebApiEmpty.Controllers
{
    public class ProductQueryController : ApiController
    {
        public IHttpActionResult Post(SearchRequest request)
        {
            if (request == null)
            {
                request = new SearchRequest("","",0,"","");
            }

            var products = ProductDataProvider.GetProducts().AsQueryable();
            IQueryable<Product> data = request.GetOrderedData(products);
            var productViewModels = data.ToList();
            return Ok(productViewModels);
        }
    }
}
