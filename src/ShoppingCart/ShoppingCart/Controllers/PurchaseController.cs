﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJSWebApiEmpty.Models;

namespace AngularJSWebApiEmpty.Controllers
{
    public class PurchaseController : ApiController
    {
        public IHttpActionResult Post(CartViewModel cart)
        {
            PurchaseViewModel purchase = new PurchaseViewModel
            {
                Cart = cart,
                ConfirmationNo = DateTime.Now.Ticks.ToString(),
                Date = DateTime.Now
            };
            return Ok(purchase);

        }
    }
}
