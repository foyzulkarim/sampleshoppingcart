﻿using System;
using System.Collections.Generic;

namespace AngularJSWebApiEmpty.Models
{
    public enum Types
    {
        Clothing, Phones, Computer, Books,
    }

    public static class ProductDataProvider
    {
        public static List<Product> GetProducts()
        {
            List<Product> products = new List<Product>
            {
                new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Gerber Baby Girls' Three-Piece Set with Bodysuit, Cap, and Pant",
                    Description =
                        "100% Cotton ,Imported, Machine Wash, Set including long-sleeve graphic bodysuit with coordinating cap and pant, Bodysuit features lap-shoulder neckline and bottom snaps,Pant features elastic waistband and ruffled ankles",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/817niasYbfL._UY879_.jpg",
                    Price = 5,
                    Type = Types.Clothing.ToString()
                },
                                new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Motorola Moto G (2nd generation) Unlocked Cellphone, 8GB, Black",
                    Description = @"Front ported stereo speakers
                        All-day 2,070 mAh battery
                        Corning Gorilla Glass 3
                        Optimized to work with 3G (UMTS/HSPA+) networks when roaming outside the US. Only supports 2G coverage on T-Mobile
                        This device is shipped with a wall charger included
                        Unlocked cell phones are compatible with GSM carriers like AT&T and T-Mobile as well as with GSM SIM cards (e.g. H20, Straight Talk, and select prepaid carriers). Unlocked cell phones will not work with CDMA Carriers like Sprint, Verizon, Boost or Virgin.
                        5-inch 720p HD display",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/41H7-P5nKPL.jpg",
                    Price = 110,
                    Type = Types.Phones.ToString()
                },
                                               new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "BLU Advance 5.0 - Unlocked Dual Sim Smartphone - US GSM - White",
                    Description = @"Unlocked Dual SIM smartphone, with Android 5.1 Lollipop
                    Mediatek 6580 Quad Core 1.3 GHz Processor with arm Mali-400 GPU
                    5.0 IPS display: 5MP Main Camera with LED flash and 2MP Camera with LED flash
                    4GB Internal memory 768MB RAM Micro SD up to 64GB
                    GSM Quad band 4G HSPA+(850 / 1700 / 1900): US compatibility Nationwide on all GSM Networks including AT & T, T - Mobile, Cricket, MetroPCS, Straight Talk, Pure Talk USA and others",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/71q-Xbyl4iL._SL1500_.jpg",
                    Price = 60,
                    Type = Types.Phones.ToString()
                },
                                                                            new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Toshiba CB35-B3330 13.3 Inch Chromebook (Intel Celeron, 2GB, 16GB SSD, Silver)",
                    Description = @"Intel Celeron 2.16 GHz Processor
                        2 GB DDR3L SDRAM
                        Can open/edit MS Office files using free embedded QuickOffice editor or Google Docs, and can download Microsoft Office Online (an online version of Microsoft Office) for free. Cannot install standard MS Office software.
                        16 GB Solid-State Drive (SSD); No CD or DVD drive
                        13.3 inch 1366x768 pixels LED-lit Screen
                        Chrome Operating System",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/81iWrNn0nmL._SL1500_.jpg",
                    Price = 250,
                    Type = Types.Computer.ToString()
                },
                                                                                                                              new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Dell Inspiron i3148-6840sLV 11.6-Inch 2 in 1 Convertible Touchscreen Laptop (Intel Core i3 Processor, 4GB RAM)",
                    Description = @"4th Generation Intel Core i3-4010U processor
                        4 GB DDR3L RAM
                        500 GB 5400 rpm Hard Drive, Built in Wireless b/g/n
                        12-inch Screen
                        Windows 8.1 (64-bit) operating system",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/41sa05nH%2BFL.jpg",
                    Price = 450,
                    Type = Types.Computer.ToString()
                },    new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Harry Potter Coloring Book",
                    Description = @"Harry Potter Coloring Book",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/81uHJU1iy9L._SL1200_.jpg",
                    Price = 10,
                    Type = Types.Books.ToString()
                },  new Product()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "The 100: A Ranking Of The Most Influential Persons In History",
                    Description = @"A list of the one hundred most influential people in history features descriptions of the careers, contributions, and accomplishments of the political and religious leaders, inventors, writers, artists, and others who changed the course of history.",
                    ImageUrl = "http://ecx.images-amazon.com/images/I/51SmlkQdllL.jpg",
                    Price = 10,
                    Type = Types.Books.ToString()
                },
            };
            return products;
        }
    }
}