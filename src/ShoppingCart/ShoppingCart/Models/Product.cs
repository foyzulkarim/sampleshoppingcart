﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSWebApiEmpty.Models
{
    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public double Price { get; set; }
        public string ImageUrl { get; set; }
    }
}
