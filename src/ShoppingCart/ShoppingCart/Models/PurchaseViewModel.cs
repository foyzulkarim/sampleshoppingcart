﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJSWebApiEmpty.Models
{
    public class PurchaseViewModel
    {
        public string ConfirmationNo { get; set; }
        public CartViewModel Cart { get; set; }
        public DateTime Date { get; set; }
    }

    public class ProductCartModel
    {
        public ProductCartModel(Product p, int qty)
        {
            this.Product = p;
            this.Quantity = qty;
        }

        public int Quantity { get; set; }

        public Product Product { get; set; }
    }

    public class CartViewModel
    {
        public CartViewModel()
        {
            this.Products = new List<ProductCartModel>();
            this.SubTotal = 0.0;
            this.DeliveryFee = 0.0;
            this.Total = 0.0;
        }

        public double Total { get; set; }

        public double DeliveryFee { get; set; }

        public double SubTotal { get; set; }

        public List<ProductCartModel> Products { get; set; }
    }
}