﻿using System;
using System.Linq.Expressions;

namespace AngularJSWebApiEmpty.Models
{
    

    public class SearchRequest : RequestModel<Product>
    {
        public string Type { get; set; }
        public double Price { get; set; }
         
        public SearchRequest(string keyword, string type, double price, string orderBy, string isAscending) : base(keyword,  orderBy, isAscending)
        {
           
            Type = type;
            Price = price;
          
        }

        protected override Expression<Func<Product, bool>> GetExpression()
        {
            if (!string.IsNullOrWhiteSpace(Keyword))
            {
                ExpressionObj = x => x.Name.ToLower().Contains(Keyword) || x.Description.ToLower().Contains(Keyword) || x.Name.ToLower().Contains(Keyword);
            }
          
            if (!string.IsNullOrWhiteSpace(Type))
            {
                ExpressionObj = ExpressionObj.And(x => x.Type.Contains(Type));
            }
            if (Price>0)
            {
                ExpressionObj = ExpressionObj.And(x => x.Price <= Price);
            }
            
            return ExpressionObj;
        }
    }
}
