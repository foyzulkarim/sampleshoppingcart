﻿// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
module App {
   
    "use strict";

    export class SaveService extends BaseService {
        static $inject: string[] = ["$q", "urlService", "webService"];

        constructor($q: angular.IQService, urlService: UrlService, webService: WebService) {
            super($q, urlService, webService);
        }

        Save(request: any, url: string): angular.IPromise<BaseResponse> {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Post(url, request).then((result: any): any => {
                var response: BaseResponse = new BaseResponse(true, result.data,"Success");
                deffered.resolve(response);
            }, error => {
                deffered.reject(error);
            });
            return deffered.promise;
        }

        Update(request: any, url: string): angular.IPromise<BaseResponse> {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Put(url, request).then((result: any): any => {
                var response: BaseResponse = new BaseResponse(true, result.data, "Success");
                deffered.resolve(response);
            }, error => {
                deffered.reject(error);
            });
            return deffered.promise;
        }

        SavePurchase(request: CartViewModel): angular.IPromise<BaseResponse> {
            var self = this;
            return self.Save(request, self.Url.PurchaseUrl);
        }

    }


    angular.module("app").service("SaveService", SaveService);
}