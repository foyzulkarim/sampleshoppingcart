var App;
(function (App) {
    var DataService = (function () {
        function DataService() {
            this.Cart = new App.CartViewModel();
        }
        return DataService;
    })();
    App.DataService = DataService;
    angular.module('app').service('DataService', DataService);
})(App || (App = {}));
//# sourceMappingURL=DataService.js.map