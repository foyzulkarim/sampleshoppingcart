﻿// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
module App {
   
    export class SearchService extends BaseService {
        static $inject: string[] = ["$q", "urlService", "webService"];
        Id: string;
        ChildId: string;
        View: string;

        constructor($q: angular.IQService, urlService: UrlService, webService: WebService) {
            super($q, urlService, webService);
            this.Id = undefined;
        }

        Search(request: SearchRequest, url: string): angular.IPromise<SearchResponse> {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Post(url, request).then((result: any): any => {
                var response: SearchResponse = new SearchResponse(result.data);
                deffered.resolve(response);
            }, error => {
                deffered.reject(error);
            });
            return deffered.promise;
        }


        SearchProduct(request: SearchRequest): angular.IPromise<SearchResponse> {
            return this.Search(request, this.Url.ProductQueryUrl);
        }
        
        LoadLookupData(): angular.IPromise<LookupDataResponse> {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Get(self.Url.LookupDataUrl).then((result: any): any => {
                var response: LookupDataResponse = new LookupDataResponse(result.data);
                deffered.resolve(response);
            }, error => {
                deffered.reject(error);
            });
            return deffered.promise;
        }

        GetDetail(url: string): angular.IPromise<any> {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Get(url).then((result: any): any => {
                deffered.resolve(result.data);
            }, error => {
                deffered.reject(error);
            });
            return deffered.promise;
        }        
    }


    angular.module("app").service("SearchService", SearchService);
}