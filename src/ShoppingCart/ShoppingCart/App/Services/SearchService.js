var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var App;
(function (App) {
    var SearchService = (function (_super) {
        __extends(SearchService, _super);
        function SearchService($q, urlService, webService) {
            _super.call(this, $q, urlService, webService);
            this.Id = undefined;
        }
        SearchService.prototype.Search = function (request, url) {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Post(url, request).then(function (result) {
                var response = new App.SearchResponse(result.data);
                deffered.resolve(response);
            }, function (error) {
                deffered.reject(error);
            });
            return deffered.promise;
        };
        SearchService.prototype.SearchProduct = function (request) {
            return this.Search(request, this.Url.ProductQueryUrl);
        };
        SearchService.prototype.LoadLookupData = function () {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Get(self.Url.LookupDataUrl).then(function (result) {
                var response = new App.LookupDataResponse(result.data);
                deffered.resolve(response);
            }, function (error) {
                deffered.reject(error);
            });
            return deffered.promise;
        };
        SearchService.prototype.GetDetail = function (url) {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Get(url).then(function (result) {
                deffered.resolve(result.data);
            }, function (error) {
                deffered.reject(error);
            });
            return deffered.promise;
        };
        SearchService.$inject = ["$q", "urlService", "webService"];
        return SearchService;
    })(App.BaseService);
    App.SearchService = SearchService;
    angular.module("app").service("SearchService", SearchService);
})(App || (App = {}));
//# sourceMappingURL=SearchService.js.map