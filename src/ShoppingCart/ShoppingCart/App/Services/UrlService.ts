﻿module App {
    "use strict";

    export class UrlService {
        constructor() {
            var baseUrl = "http://localhost:3027";
            baseUrl = "";
            var baseApi = baseUrl + "/api";
            this.ProductQueryUrl = baseApi + "/ProductQuery";
            this.PurchaseUrl = baseApi + "/Purchase";
        }

        LookupDataUrl: string;
        ProductQueryUrl: string;
        PurchaseUrl: string;
    }

    angular.module("app").service("urlService", UrlService);
}