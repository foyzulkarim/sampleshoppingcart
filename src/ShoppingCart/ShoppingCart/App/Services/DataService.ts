﻿module App {
    export class DataService {
        constructor() {
            this.Cart = new CartViewModel();
        }

        Cart:CartViewModel;
    }

    angular.module('app').service('DataService', DataService);
}