var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var App;
(function (App) {
    "use strict";
    var SaveService = (function (_super) {
        __extends(SaveService, _super);
        function SaveService($q, urlService, webService) {
            _super.call(this, $q, urlService, webService);
        }
        SaveService.prototype.Save = function (request, url) {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Post(url, request).then(function (result) {
                var response = new App.BaseResponse(true, result.data, "Success");
                deffered.resolve(response);
            }, function (error) {
                deffered.reject(error);
            });
            return deffered.promise;
        };
        SaveService.prototype.Update = function (request, url) {
            var self = this;
            var deffered = self.Q.defer();
            self.Web.Put(url, request).then(function (result) {
                var response = new App.BaseResponse(true, result.data, "Success");
                deffered.resolve(response);
            }, function (error) {
                deffered.reject(error);
            });
            return deffered.promise;
        };
        SaveService.prototype.SavePurchase = function (request) {
            var self = this;
            return self.Save(request, self.Url.PurchaseUrl);
        };
        SaveService.$inject = ["$q", "urlService", "webService"];
        return SaveService;
    })(App.BaseService);
    App.SaveService = SaveService;
    angular.module("app").service("SaveService", SaveService);
})(App || (App = {}));
//# sourceMappingURL=SaveService.js.map