var App;
(function (App) {
    "use strict";
    var UrlService = (function () {
        function UrlService() {
            var baseUrl = "http://localhost:3027";
            baseUrl = "";
            var baseApi = baseUrl + "/api";
            this.ProductQueryUrl = baseApi + "/ProductQuery";
            this.PurchaseUrl = baseApi + "/Purchase";
        }
        return UrlService;
    })();
    App.UrlService = UrlService;
    angular.module("app").service("urlService", UrlService);
})(App || (App = {}));
//# sourceMappingURL=UrlService.js.map