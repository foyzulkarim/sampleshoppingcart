﻿module App {
    "use strict";

    export class BaseResponse {
        IsSuccess: boolean;
        Data: any;
        Message: string;

        constructor(isSuccess: boolean, data: any, message: string) {
            this.IsSuccess = isSuccess;
            this.Data = data;
            this.Message = message == null ? "Success" : message;
        }
    }

    export class PermissionResponse extends BaseResponse {
        IsAllowed: boolean;
    }

    export class RegisterResponse extends BaseResponse {
        IsRegistered: boolean;
        UserName: string;
    }

    export class ErrorResponse extends BaseResponse {
        Exception: string;
    }

    export class SearchResponse extends BaseResponse {
        constructor(data: any) {
            super(true, data, "Success");
            this.Models = data;
        }
        Models: Object[];
    }

    
    export class LookupDataResponse extends BaseResponse {
        constructor(data: any) {
            super(true, data, "Success");
            this.LookupData = data;

        }
        LookupData: LookupDataViewModel;
    }
     
}
