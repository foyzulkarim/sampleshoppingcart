var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var App;
(function (App) {
    "use strict";
    var BaseViewModel = (function () {
        function BaseViewModel() {
        }
        return BaseViewModel;
    })();
    App.BaseViewModel = BaseViewModel;
    var ProductViewModel = (function (_super) {
        __extends(ProductViewModel, _super);
        function ProductViewModel() {
            _super.apply(this, arguments);
        }
        return ProductViewModel;
    })(BaseViewModel);
    App.ProductViewModel = ProductViewModel;
    var ProductCartModel = (function (_super) {
        __extends(ProductCartModel, _super);
        function ProductCartModel(p, qty) {
            _super.call(this);
            this.Product = p;
            this.Quantity = qty;
        }
        ProductCartModel.prototype.Total = function () {
            return this.Product.Price * this.Quantity;
        };
        return ProductCartModel;
    })(BaseViewModel);
    App.ProductCartModel = ProductCartModel;
    var LookupDataViewModel = (function () {
        function LookupDataViewModel() {
            this.Types = [];
        }
        return LookupDataViewModel;
    })();
    App.LookupDataViewModel = LookupDataViewModel;
    var CartViewModel = (function () {
        function CartViewModel() {
            this.Products = [];
            this.SubTotal = 0;
            this.DeliveryFee = 0;
            this.Total = 0;
        }
        return CartViewModel;
    })();
    App.CartViewModel = CartViewModel;
    var PurchaseViewModel = (function () {
        function PurchaseViewModel() {
            this.ConfirmationNo = "";
            this.Cart = new CartViewModel();
            this.Date = new Date();
        }
        return PurchaseViewModel;
    })();
    App.PurchaseViewModel = PurchaseViewModel;
})(App || (App = {}));
//# sourceMappingURL=View.Models.js.map