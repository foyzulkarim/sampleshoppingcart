﻿module App {
    "use strict";

    export class SigninRequest {
        Email: string;
        Password: string;

        constructor(email: string, password: string) {
            this.Email = email;
            this.Password = password;
        }
    }

    export class RegisterRequest {
        Email: string;
        Password: string;
        ConfirmPassword: string;

        constructor(email: string, password: string, confirmPassword: string) {
            this.Email = email;
            this.Password = password;
            this.ConfirmPassword = confirmPassword;
        }
    }

    export class AccountInfo {
        UserName: string;
        AuthToken: string;
        AccessToken: string;
        IsAuth: boolean;
    }

    export class PermissionRequest {
        Name: string;

        constructor(name: string) {
            this.Name = name;
        }
    }

    export class Notification {
        IsError: boolean;
        IsInfo: boolean;
        Message: string;
    }

    export class DataRequest {
        Page: number;
        OrderBy: string;
        Keyword: string;
        IsAscending: string;

        protected getBaseQueryString(): string {
            if (this.Page == null) this.Page = 1;
            var queryString = `?keyword=${this.Keyword}&orderBy=${this.OrderBy}&page=${this.Page}`;
            return queryString;
        }
    }

    export class SearchRequest extends DataRequest {
        constructor(keyword: string, type: string, price: number,  page: number, orderBy: string, isAsc: string) {
            super();
            this.Keyword = keyword;
            this.Type = type;
            this.Price = price;     
            this.Page = page;
            this.OrderBy = orderBy;
            this.IsAscending = isAsc;
               
        }

        Type: string;
        Price: number;
        Quantity: number;
    }


    export class DetailRequest extends DataRequest {
        Id: string;

        constructor(id: string) {
            this.Id = id;
            super();
        }

        GetQueryString(): string {
            return '?id=' + this.Id;
        }
    }






}