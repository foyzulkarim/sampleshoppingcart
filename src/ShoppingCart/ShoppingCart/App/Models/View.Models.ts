﻿module App {
    "use strict";

    export class BaseViewModel {
        Id: string;
    }

    export class ProductViewModel extends BaseViewModel {
        Name: string;
        Description: string;
        Price: number;
        Type: string;
        ImageUrl: string;
    }

    export class ProductCartModel extends BaseViewModel {
        constructor(p: ProductViewModel, qty: number) {
            super();
            this.Product = p;
            this.Quantity = qty;
        }
        Total(): number {
            return this.Product.Price * this.Quantity;
        }
        Quantity: number;
        Product: ProductViewModel;

    }

    export class LookupDataViewModel {
        constructor() {
            this.Types = [];
        }
        Types: string[];
    }


    export class CartViewModel {
        constructor() {
            this.Products = [];
            this.SubTotal = 0;
            this.DeliveryFee = 0;
            this.Total = 0;

        }

        Products: ProductCartModel[];
        SubTotal: number;
        DeliveryFee: number;
        Total: number;

    }

    export class PurchaseViewModel {
        constructor() {
            this.ConfirmationNo = "";
            this.Cart = new CartViewModel();
            this.Date = new Date();
        }

        ConfirmationNo: string;
        Cart: CartViewModel;
        Date: Date;
    }

}