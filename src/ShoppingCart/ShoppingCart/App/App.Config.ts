﻿module App {
    "use strict";

    export class AppConfig {

        static $inject = ["$stateProvider", "$urlRouterProvider"];

        constructor($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) {
            console.log("I am in config constructor.");

            $stateProvider
                .state("root", {
                    abstract: true,
                    url: "",
                    //template:""
                    template: "<div ui-view></div>"
                })
                .state("root.home", {
                    url: "/",
                    views: {
                        "":
                        {
                            templateUrl: "partials/products.tpl.html",
                            controller: 'HomeController',
                            controllerAs:'vm'
                        }
                    }
                }).state("root.cartreview", {
                    url: "/",
                    views: {
                        "":
                        {
                            templateUrl: "partials/cartReview.tpl.html",
                            controller: 'CartReviewController',
                            controllerAs: 'vm'
                        }
                    }
                }).state("root.purchase", {
                    url: "/",
                    views: {
                        "":
                        {
                            templateUrl: "partials/purchase.tpl.html",
                            controller: 'PurchaseController',
                            controllerAs: 'vm'
                        }
                    }
                });


            $urlRouterProvider.otherwise("/");
        }
    }

    angular.module("app", ["ngResource", "ui.router"]);
    angular.module("app").config(AppConfig);
}