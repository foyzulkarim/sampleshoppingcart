var App;
(function (App) {
    "use strict";
    var AppConfig = (function () {
        function AppConfig($stateProvider, $urlRouterProvider) {
            console.log("I am in config constructor.");
            $stateProvider
                .state("root", {
                abstract: true,
                url: "",
                template: "<div ui-view></div>"
            })
                .state("root.home", {
                url: "/",
                views: {
                    "": {
                        templateUrl: "partials/products.tpl.html",
                        controller: 'HomeController',
                        controllerAs: 'vm'
                    }
                }
            }).state("root.cartreview", {
                url: "/",
                views: {
                    "": {
                        templateUrl: "partials/cartReview.tpl.html",
                        controller: 'CartReviewController',
                        controllerAs: 'vm'
                    }
                }
            }).state("root.purchase", {
                url: "/",
                views: {
                    "": {
                        templateUrl: "partials/purchase.tpl.html",
                        controller: 'PurchaseController',
                        controllerAs: 'vm'
                    }
                }
            });
            $urlRouterProvider.otherwise("/");
        }
        AppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
        return AppConfig;
    })();
    App.AppConfig = AppConfig;
    angular.module("app", ["ngResource", "ui.router"]);
    angular.module("app").config(AppConfig);
})(App || (App = {}));
//# sourceMappingURL=App.Config.js.map