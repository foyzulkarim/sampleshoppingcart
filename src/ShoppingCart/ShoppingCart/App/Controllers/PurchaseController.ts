// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
module App {
    "use strict";

    export class PurchaseController {
        Purchase: PurchaseViewModel;
        static $inject: string[] = ["$state", "DataService", "SaveService"];
        private state: angular.ui.IStateService;
        private data: DataService;
        private save: SaveService;
        constructor(private $state: angular.ui.IStateService, data: DataService, save: SaveService) {
            this.state = $state;
            this.data = data;
            this.save = save;
          //  this.Purchase = new PurchaseViewModel();
            this.Activate();
        }

        Activate() {
            var self = this;
            this.save.SavePurchase(this.data.Cart)
                .then(x => {
                    self.Purchase = <PurchaseViewModel>(x.Data);
                    console.log(self.Purchase);
                },
                error => {
                    console.log(error);
                });
        }
    }

    angular.module("app").controller("PurchaseController", PurchaseController);
}