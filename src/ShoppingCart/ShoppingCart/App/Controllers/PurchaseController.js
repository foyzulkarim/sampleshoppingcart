var App;
(function (App) {
    "use strict";
    var PurchaseController = (function () {
        function PurchaseController($state, data, save) {
            this.$state = $state;
            this.state = $state;
            this.data = data;
            this.save = save;
            this.Activate();
        }
        PurchaseController.prototype.Activate = function () {
            var self = this;
            this.save.SavePurchase(this.data.Cart)
                .then(function (x) {
                self.Purchase = (x.Data);
                console.log(self.Purchase);
            }, function (error) {
                console.log(error);
            });
        };
        PurchaseController.$inject = ["$state", "DataService", "SaveService"];
        return PurchaseController;
    })();
    App.PurchaseController = PurchaseController;
    angular.module("app").controller("PurchaseController", PurchaseController);
})(App || (App = {}));
//# sourceMappingURL=PurchaseController.js.map