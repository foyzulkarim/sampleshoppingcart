var App;
(function (App) {
    "use strict";
    var CartReviewController = (function () {
        function CartReviewController(state, data) {
            this.state = state;
            this.data = data;
            this.Activate();
        }
        CartReviewController.prototype.Activate = function () {
            console.log('i m in cart review controller');
            this.Cart = this.data.Cart;
        };
        CartReviewController.$inject = ["$state", "DataService"];
        return CartReviewController;
    })();
    App.CartReviewController = CartReviewController;
    angular.module("app").controller("CartReviewController", CartReviewController);
})(App || (App = {}));
//# sourceMappingURL=CartReviewController.js.map