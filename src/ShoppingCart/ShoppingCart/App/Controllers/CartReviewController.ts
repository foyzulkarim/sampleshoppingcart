// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
module App {
    "use strict";

    export class CartReviewController {

        static $inject: string[] = ["$state", "DataService"];
        private state: angular.ui.IStateService;
        private data: DataService;
        Cart: CartViewModel;
        constructor(state: ng.ui.IStateService, data: DataService) {
            this.state = state;
            this.data = data;
            this.Activate();

        }

        Activate() {
            console.log('i m in cart review controller');
            this.Cart = this.data.Cart;
        }
    }

    angular.module("app").controller("CartReviewController", CartReviewController);
}