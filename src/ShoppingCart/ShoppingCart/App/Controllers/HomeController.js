var App;
(function (App) {
    "use strict";
    var HomeController = (function () {
        function HomeController(search, dataService, state) {
            var _this = this;
            this.Title = "HomeController";
            this.productExistsInCart = function (product) {
                return _this.Cart.Products.filter(function (x) { return x.Product.Name === product.Name; }).length > 0;
            };
            this.SearchService = search;
            this.data = dataService;
            this.state = state;
            this.Init();
        }
        HomeController.prototype.Init = function () {
            this.Cart = new App.CartViewModel();
            if (this.data.Cart) {
                this.Cart = this.data.Cart;
            }
            this.Products = [];
            this.LookupData = new App.LookupDataViewModel();
            this.LookupData.Types.push("All");
            this.LookupData.Types.push("Clothing");
            this.LookupData.Types.push("Phones");
            this.LookupData.Types.push("Computer");
            this.LookupData.Types.push("Books");
            this.SearchRequest = new App.SearchRequest("", "", 0, 0, "Name", "True");
            console.log('this is test home controller');
            this.Search();
        };
        HomeController.prototype.Search = function () {
            var self = this;
            var successCallback = function (response) {
                console.log(response);
                self.Products = (response.Models);
            };
            var errorCallback = function (error) {
                console.log(error);
            };
            self.SearchService.SearchProduct(self.SearchRequest).then(successCallback, errorCallback);
        };
        HomeController.prototype.ToggleSelection2 = function (key, value) {
            if (value === "All") {
                value = "";
            }
            this.SearchRequest[key] = value;
            console.log(this.SearchRequest);
            this.Search();
        };
        HomeController.prototype.SelectOrderBy = function (orderBy) {
            this.SearchRequest.OrderBy = orderBy;
            console.log(this.SearchRequest);
            this.Search();
        };
        HomeController.prototype.SelectOrderIsAsc = function (isAsc) {
            this.SearchRequest.IsAscending = isAsc;
            console.log(this.SearchRequest);
            this.Search();
        };
        HomeController.prototype.AddToCart = function (product) {
            if (this.productExistsInCart(product)) {
                this.Cart.Products.filter(function (x) { return x.Product.Name === product.Name; })[0].Quantity += 1;
            }
            else {
                this.Cart.Products.push(new App.ProductCartModel(product, 1));
            }
            this.updateCartTotal();
        };
        HomeController.prototype.RemoveFromCart = function (product) {
            var cartItem = this.Cart.Products.filter(function (x) { return x.Product.Name === product.Name; })[0];
            var indexOfProduct = this.Cart.Products.indexOf(cartItem);
            this.Cart.Products.splice(indexOfProduct, 1);
            this.updateCartTotal();
        };
        HomeController.prototype.updateCartTotal = function () {
            var _this = this;
            var self = this;
            this.Cart.SubTotal = 0;
            self.Cart.DeliveryFee = 0;
            self.Cart.Total = 0;
            self.Cart.Products.forEach(function (p) { return _this.Cart.SubTotal += p.Total(); });
            if (self.Cart.SubTotal < 10 && self.Cart.SubTotal > 0) {
                self.Cart.DeliveryFee = 5;
            }
            self.Cart.Total = self.Cart.SubTotal + self.Cart.DeliveryFee;
            console.log(self.Cart);
        };
        HomeController.prototype.GoToReview = function () {
            this.data.Cart = this.Cart;
            this.state.go('root.cartreview');
        };
        HomeController.$inject = ["SearchService", "DataService", "$state"];
        return HomeController;
    })();
    App.HomeController = HomeController;
    angular.module("app").controller("HomeController", HomeController);
})(App || (App = {}));
//# sourceMappingURL=HomeController.js.map