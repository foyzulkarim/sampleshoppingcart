// Install the angularjs.TypeScript.DefinitelyTyped NuGet package
module App {
    
    "use strict";

    export class HomeController {
        Title: string = "HomeController";
        SearchService: SearchService;
        static $inject: string[] = ["SearchService","DataService","$state"];
        LookupData: LookupDataViewModel;
        SearchRequest: SearchRequest;
        Products: ProductViewModel[];
        Cart: CartViewModel;
        private data: DataService;
        private state: angular.ui.IStateService;

        constructor(search: SearchService, dataService: DataService, state: angular.ui.IStateService) {
            this.SearchService = search;
            this.data = dataService;
            this.state = state;
            this.Init();
        }

        Init() {
            this.Cart = new CartViewModel();
            if (this.data.Cart) {
                this.Cart = this.data.Cart;
            }
            this.Products = [];
            this.LookupData = new LookupDataViewModel();
            this.LookupData.Types.push("All");
            this.LookupData.Types.push("Clothing");
            this.LookupData.Types.push("Phones");
            this.LookupData.Types.push("Computer");
            this.LookupData.Types.push("Books");

            this.SearchRequest = new SearchRequest("", "", 0, 0, "Name", "True");
            console.log('this is test home controller');
            this.Search();
        }


        Search(): void {
            var self = this;
            var successCallback = (response: SearchResponse): void => {
                console.log(response);
                self.Products = <ProductViewModel[]>(response.Models);
            };
            var errorCallback = (error: any): void => {
                console.log(error);
            };
            self.SearchService.SearchProduct(self.SearchRequest).then(successCallback, errorCallback);
        }


        ToggleSelection2(key: string, value: string): void {
            if (value === "All") {
                value = "";
            }
            this.SearchRequest[key] = value;
            console.log(this.SearchRequest);
            this.Search();
        }

        SelectOrderBy(orderBy: string): void {
            this.SearchRequest.OrderBy = orderBy;
            console.log(this.SearchRequest);
            this.Search();
        }

        SelectOrderIsAsc(isAsc: string): void {
            this.SearchRequest.IsAscending = isAsc;
            console.log(this.SearchRequest);
            this.Search();
        }

        private productExistsInCart = (product: ProductViewModel) => {
            return this.Cart.Products.filter(x => x.Product.Name === product.Name).length > 0;
        };

        AddToCart(product: ProductViewModel): void {
            
            if (this.productExistsInCart(product)) {
                this.Cart.Products.filter(x => x.Product.Name === product.Name)[0].Quantity += 1;
            } else {
                this.Cart.Products.push(new ProductCartModel(product, 1));
            }                       

            this.updateCartTotal();
        }

        RemoveFromCart(product: ProductViewModel): void {
            var cartItem = this.Cart.Products.filter(x => x.Product.Name === product.Name)[0];
            var indexOfProduct = this.Cart.Products.indexOf(cartItem);
            this.Cart.Products.splice(indexOfProduct, 1);
            this.updateCartTotal();
        }
        
        private updateCartTotal(): void {
            var self = this;
            this.Cart.SubTotal = 0;
            self.Cart.DeliveryFee = 0;
            self.Cart.Total = 0;

            self.Cart.Products.forEach(p => this.Cart.SubTotal += p.Total());
            if (self.Cart.SubTotal < 10 && self.Cart.SubTotal>0) {
                self.Cart.DeliveryFee = 5;
            }
            self.Cart.Total = self.Cart.SubTotal + self.Cart.DeliveryFee;
            console.log(self.Cart);
        }

        GoToReview(): void {
            this.data.Cart = this.Cart;
            this.state.go('root.cartreview');
        }

    }

    angular.module("app").controller("HomeController", HomeController);
}